/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package serviorfigura;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Angel
 */
public class Server extends javax.swing.JDialog {
    
    private String inputLine;
    private DataInputStream entrada;
    private final int PUERTO = 5432;
    private ServerSocket serverSocket = null;

    public Server(java.awt.Frame parent, boolean modal) {
        
        super(parent, modal);
        initComponents();
    }
    public class Figura extends Thread {

        public void run() {
            
            try {
                serverSocket = new ServerSocket(PUERTO);
            } catch (IOException e) {
                System.err.println("No se puede escuchar por el puerto:" + PUERTO);
                System.exit(1);
            }
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
                entrada = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));

            } catch (IOException e) {
                System.err.println("Conexion fallida");
                System.exit(1);
            }
            while (true) {
                try {
                   Graphics g = getGraphics();
                    inputLine = entrada.readLine();
                    paint(g, inputLine);
                } catch (IOException e) {
                    System.err.println("La figura no se pudo dibujar");
                }
            }
        }

        public void paint(Graphics g, String ipl) {
            switch (ipl) {
                case "11":
                    g.setColor(Color.blue);
                    g.fillOval(50, 100, 200, 100);

                    break;
                case "12":
                    g.setColor(Color.green);
                    g.fillOval(50, 100, 200, 200);
                    break;
                case "13":
                    g.setColor(Color.red);
                    g.fillOval(50, 100, 200, 200);

                    break;
                case "21":
                    g.setColor(Color.blue);
                    g.fillRect(50, 100, 300, 200);

                    break;
                case "22":
                    g.setColor(Color.green);
                    g.fillRect(50, 100, 300, 200);
                    break;
                case "23":
                    g.setColor(Color.red);
                    g.fillRect(50, 100, 300, 200);
                    break;
                default:
                    System.out.println("No se encuentra el color");
                    break;
            }
            
            
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnServicio = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnServicio.setText("Iniciar Servicio");
        btnServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServicioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addComponent(btnServicio)
                .addContainerGap(162, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(271, Short.MAX_VALUE)
                .addComponent(btnServicio)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServicioActionPerformed
      Figura figura = new Figura();
    
         figura.start();
    }//GEN-LAST:event_btnServicioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Server.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Server dialog = new Server(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });

                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnServicio;
    // End of variables declaration//GEN-END:variables
}
